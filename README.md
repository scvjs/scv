# Scv.js

Store / Controller / View 패턴 프레임워크


__browser sync(only)__  
__low learning curve__  
virutal dom  
high performace  



Get help from other Scv.js users:

* [forum.red on forum](https://forum.red/scvjs)


_If you have other helpful links to share, or find any of the links above no longer work, please [let us know](https://github.com/sungu-che/scv/issues)._

## Learning scv.js
##### intro guide
	intro guide


## Get Started & API


	
###### [SCV](#scv)
	// template style - inner script
   
		var todoItemTpl = '<div class="view ">\
  						<input class="toggle" { this.complete } @click="checked" type="checkbox"> <label>{this.title}</label> <button @click="remove" class="destroy"></button>\
  					</div>';

	// template style - inner html

		<script id="todoItemTpl" type="text/template">
			<div class="view ">
				<input class="toggle" { this.complete } @click="checked" type="checkbox"> <label>{this.title}</label> <button @click="remove" class="destroy"></button>
			</div>
		</script>
		
	/* init */

	scv({
		id : "todo_item",
		sync: true,
		template : todoItemTpl,
		target : document.querySelector(".todo-list"), // or todoItemTpl
		action : {
			checked : completeTodo,
			remove : removeTodo
		}
	});

	


#### [getItem](#getItem)
 
	/*
		{ parameter } 
		id   : id name,
		idx  : item index
	*/
	
	var p = scv.getItem({id:"id_name"});
	
	console.log(p);
	{
	
	}

  
#### [addItem](#addItem)
	/*
		{ parameter } 
		id   : id name
		data : object & array
	*/

	var data = {}; // or var data = [];
	scv.addItem({id:"id_name", data : data})

#### [setItem](#setItem)
	/*
		{ parameter } 
		id   : id name
		idx  : index number
		data : object & array
	*/
	
	var data = {}; // or var data = [];
	scv.setItem({id:"id_name", idx : 0, data : data})

#### [removeItem](#removeItem)
	/*
		{ parameter } 
		id   : id name
		idx  : item index
	*/
	
	scv.removeItem({id:"id_name", idx : 0})


  
  
## Credit

This TodoMVC application was created by [SungU Che](http://forum.red/@sungu.che).
